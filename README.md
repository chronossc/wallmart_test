# Wallmart test project - Overview

This is a project that Wallmart requires as test for it's employment process.

This project should allow add new routes to database and allow to given two points, truck autonomy and fuel price calculate best route to delivery.

# Motivations and packages

This project will use Django and Django Rest Framework to deliver a REST API to NEO4J database.

Django is here because Django REST Framework, we can easily do that API in Flask too, but Django REST Framework is amazing to design REST APIs and will help with small time of the project. For this project basic stuff of Django like authentication, admin and sessions was disabled.

Django Rest Framework give us a nice REST API without much work allowing us to focus on real problem, the calculation of best routes.

NEO4J is a graph database, fast and with shortest path algorithms built in. At start I was implementing Dijkstra's shortest path algorithm using MongoDB but NEO4J in this case is pretty easy to work and maintain. A wrapper called NeoModel was used to provide a 'ORM' like interface.

# Project setup.

## Install packages

Just clone the project into a new virtualenv and install the files in requirements:

` pip install -r requirements `

## Configure database

I'm using SQLite database on project directory because I'm on a SSD and we even use Django stuff. Please, set up any database that guys prefer on settings.

Also, there is NEO4J. I have made a bash script to set it up for us. Run `setup_neo4j.sh` from project root and it will put a NEO4J server running.

## Run the tests

`setup_neo4j.sh` will set up the NEO4J server, but for tests we want another instance running on port 7475 instead 7474 and with a extension to help in unit tests. From project root run:

```
neo4j/neo4j-test/bin/neo4j console
```

Now, in another shell, run:

```
NEO4J_REST_URL="http://localhost:7475/db/data/" ./manage.py test -v2
```

In future we must mock the DB so we don't need to run it.

## Run the wsgi server

For server I choose to run the project on uwsgi. There is a executable `run_uwsgi` on this folder, just run it. It will use environment python2.7.

The project will be served at http://localhost:8080/.

Note: You don't need uwsgi to run unit tests.

# API documentation

For this project we will have any authentication system, but if we have, will use JWT Tokens with: http://getblimp.github.io/django-rest-framework-jwt/

The data will be exchanged in JSON format and you can play with curl or Postman. Samples with curl will be added.

The samples will not have host and port and path, and so `http://localhost:8080/api/places/` will be referenced as `$SERVER/api/places/`.

To play with curl we will use an alias to add headers and will set SERVER variable:

```bash
alias curl='curl -H "Accept: application/json" -H "Content-Type: application/json"'
SERVER="http://localhost:8080"
```

The project has a file to import in postman too.

## List places

This returns a list of existent places.

CURL request:

```bash
curl -X GET $SERVER/api/places/
```

Successful response:

```json
// HTTP 200 OK
{
  "count": 19,
  "next": null,
  "previous": null,
  "results": [
    { "map": "SP", "name": "Jd Apura", "slug": "sp--jd-apura" },
    { "map": "SP", "name": "Pedreira", "slug": "sp--pedreira" },
    { "map": "SP", "name": "Cidade Ademar", "slug": "sp--cidade-ademar" },
    { "map": "SP", "name": "Jd Jabaquara", "slug": "sp--jd-jabaquara" },
    { "map": "SP", "name": "Vila Sao Paulo", "slug": "sp--vila-sao-paulo" },
    { "map": "SP", "name": "Diadema", "slug": "sp--diadema" },
    { "map": "SP", "name": "Metro Jabaquara", "slug": "sp--metro-jabaquara" },
    { "map": "SP", "name": "Inicio Imigrantes", "slug": "sp--inicio-imigrantes" },
    { "map": "SP", "name": "Congonhas", "slug": "sp--congonhas" },
    { "map": "SP", "name": "Cruz Vermelha", "slug": "sp--cruz-vermelha" },
    { "map": "SP", "name": "Praça da Arvore", "slug": "sp--praca-da-arvore" },
    { "map": "SP", "name": "Planalto Paulista", "slug": "sp--planalto-paulista" },
    { "map": "SP", "name": "Santa Cruz", "slug": "sp--santa-cruz" },
    { "map": "SP", "name": "Moema", "slug": "sp--moema" },
    { "map": "SP", "name": "Pq Ibirapuera", "slug": "sp--pq-ibirapuera" },
    { "map": "SP", "name": "Vila Mariana - Casa", "slug": "sp--vila-mariana-casa" },
    { "map": "SP", "name": "Vila Olimpia", "slug": "sp--vila-olimpia" },
    { "map": "SP", "name": "Berrini", "slug": "sp--berrini" },
    { "map": "SP", "name": "Pq do Povo", "slug": "sp--pq-do-povo" }
  ]
}

```

## Add places

This add a new place.

CURL request:

```bash
curl -X POST --data-raw '{"map": "$MAP", "name": "$NAME"}' $SERVER/api/places/
```

Successful response:

```json
// curl -X POST --data-raw '{"map": "SP", "name": "Metro Consolação"}' $SERVER/api/places/
// HTTP 201 CREATED
{
  "map": "SP",
  "name": "Metro Consolação",
  "slug": "sp--metro-consolacao"
}
```

## Add routes

To add a route you need to know slugs from two places.

Curl request:

```bash
curl -X POST --data-raw '{"distance": "$DISTANCE"}' $SERVER/api/places/$FROM/$TO/
```

Successful response:

```json
// curl -X POST --data-raw '{"distance": "2.1"}' $SERVER/api/places/sp--vila-olimpia/sp--berrini/
// HTTP 201 CREATED
{
  "to": { "map": "SP", "name": "Berrini", "slug": "sp--berrini" },
  "from": { "map": "SP", "name": "Vila Olimpia", "slug": "sp--vila-olimpia" },
  "distance": 2.1
}
```
# Query routes

To query a route you need to know slugs from two places.

Curl request:

```bash
curl -X GET $SERVER/api/places/$FROM/$TO/$AUTONOMY/$FUEL_PRICE/
```

```json
// curl -G $SERVER/api/places/sp--jd-apura/sp--vila-mariana-casa/10/2.5/
// HTTP 200 OK
{
  "autonomy": "10.00",
  "cost": "50.25",
  "distance": "20.10",
  "fuel_price": "2.50",
  "path": [
    { "map": "SP", "name": "Jd Apura", "slug": "sp--jd-apura" },
    { "map": "SP", "name": "Pedreira", "slug": "sp--pedreira" },
    { "map": "SP", "name": "Diadema", "slug": "sp--diadema" },
    { "map": "SP", "name": "Inicio Imigrantes", "slug": "sp--inicio-imigrantes" },
    { "map": "SP", "name": "Vila Mariana - Casa", "slug": "sp--vila-mariana-casa" }
  ]
}
```