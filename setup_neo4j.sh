#!/bin/bash

CURRENT_PATH=$(pwd)
java -version 2>&1 | grep "openjdk version \"1.8" \
	&& NEO4J_CLEAN_EXTENSION="https://github.com/downloads/jexp/neo4j-clean-remote-db-addon/test-delete-db-extension-1.7.jar"
java -version 2>&1 | grep "openjdk version \"1.7" \
	&& NEO4J_CLEAN_EXTENSION="https://github.com/downloads/jexp/neo4j-clean-remote-db-addon/test-delete-db-extension-1.8.jar"
NEO4J_CLEAN_EXTENSION="https://github.com/downloads/jexp/neo4j-clean-remote-db-addon/test-delete-db-extension-1.8.jar"
NEO4J_LINK="http://neo4j.com/artifact.php?name=neo4j-community-2.2.5-unix.tar.gz"
NEO4J_TAR_FILE="${NEO4J_LINK##*name=}"
NEO4J_DIR="${CURRENT_PATH}/neo4j"
NEO4J_UNPACKED_DIR="${NEO4J_DIR}/neo4j-community-2.2.5"
NEO4J_TEST_DIR="${NEO4J_DIR}/neo4j-test"

function check_md5 {
	md5sum -c ${NEO4J_DIR}/MD5SUMS &>/dev/null && return 0 || return 1
}

[[ -d $NEO4J_DIR ]] || mkdir $NEO4J_DIR &>/dev/null
echo "cba889ad6e70f27eb28275ed84ffabe9  neo4j-community-2.2.5-unix.tar.gz" > $NEO4J_DIR/MD5SUMS
cd $NEO4J_DIR;
check_md5 || (rm $NEO4J_TAR_FILE &>/dev/null;wget "$NEO4J_LINK" -O "${NEO4J_DIR}/${NEO4J_TAR_FILE}")

if [[ check_md5 ]];
then
	tar zxvf ${NEO4J_TAR_FILE}
	sed -i -e "s/dbms.security.auth_enabled=true/dbms.security.auth_enabled=false/" $NEO4J_UNPACKED_DIR/conf/neo4j-server.properties
	cp -ax $NEO4J_UNPACKED_DIR $NEO4J_TEST_DIR

	# prepare stuff for test server
	wget "$NEO4J_CLEAN_EXTENSION" -O "${NEO4J_TEST_DIR}/plugins/${NEO4J_CLEAN_EXTENSION##*neo4j-clean-remote-db-addon\/}"
	sed -i -e "s/7474/7475/" -e "s/7473/7476/" $NEO4J_TEST_DIR/conf/neo4j-server.properties
	sed -i -e "s/7474/7475/" -e "s/7473/7476/" $NEO4J_TEST_DIR/bin/neo4j
	grep "clean extension" $NEO4J_TEST_DIR/conf/neo4j-server.properties \
		|| cat  >> $NEO4J_TEST_DIR/conf/neo4j-server.properties << EOF
# clean extension
org.neo4j.server.thirdparty_jaxrs_classes=org.neo4j.server.extension.test.delete=/db/data/cleandb
org.neo4j.server.thirdparty.delete.key=secret-key
EOF

	# run main server
	$NEO4J_UNPACKED_DIR/bin/neo4j console
else
	echo "Failure on download NEO4J" && cd $CURRENT_PATH && exit 1
fi

