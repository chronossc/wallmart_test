# -*- coding: UTF-8 -*-

from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

from django.conf.urls import url
# from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from .views import place_list, place_routes

urlpatterns = format_suffix_patterns([
    url(r'^places/$', place_list, name='place-list'),
    # url(r'^places/(?P<_from>[\w\s-]+)/(?P<to>[\w\s-]+)/$', place_routes,
    #     name='place-routes'),
    url(r'^places/(?P<_from>[\w\s-]+)/(?P<to>[\w\s-]+)/((?P<autonomy>[.0-9]+)/(?P<fuel>[.0-9]+)/)?$', place_routes,
        name='place-routes'),
])
