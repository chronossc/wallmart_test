# -*- coding: UTF-8 -*-

from __future__ import (
    absolute_import, division, print_function, unicode_literals
)
from rest_framework import status
from rest_framework.response import Response
# from rest_framework.viewsets import GenericViewSet, ViewSet, ModelViewSet
from rest_framework.generics import ListCreateAPIView, GenericAPIView
from routes.models import Place

from .serializers import PlacesSerializer


class PlacesListCreateAPIView(ListCreateAPIView):
    queryset = Place.nodes.all()
    serializer_class = PlacesSerializer


class PlacesRoutesAPIView(GenericAPIView):
    queryset = Place.nodes.all()

    def post(self, request, _from, to, *args, **kwargs):
        _from = Place.nodes.get(slug=_from)
        to = Place.nodes.get(slug=to)
        res = _from.routes.connect(to, request.data)
        return Response({
            'from': PlacesSerializer(_from).data,
            'to': PlacesSerializer(to).data,
            'distance': res.distance
        }, status=status.HTTP_201_CREATED)

    def get(self, request, _from, to, autonomy, fuel, *args, **kwargs):
        _from = Place.nodes.get(slug=_from)
        to = Place.nodes.get(slug=to)
        result = _from.get_path_to(to)
        autonomy = float(autonomy)
        fuel = float(fuel)
        if len(result) == 1:
            path = result[0].step
            data = {'path': map(lambda n: n.get_properties(), path.nodes),}
            distance = 0
            cost = 0
            for rel in path.rels:
                distance += rel.get_properties()['distance']
            cost = distance * fuel
            data.update({
                'distance': '{:.2f}'.format(distance),
                'cost': '{:.2f}'.format(cost),
                'autonomy': '{:.2f}'.format(autonomy),
                'fuel_price': '{:.2f}'.format(fuel),
            })
            return Response(data)
        else:
            return Response({'path': [], 'distance': '0', 'cost': '0'})


place_list = PlacesListCreateAPIView.as_view()
place_routes = PlacesRoutesAPIView.as_view()
