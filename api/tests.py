# -*- coding: UTF-8 -*-

from __future__ import (
    absolute_import, division, print_function, unicode_literals
)
# import os
import warnings
import json
from neomodel.core import db

warnings.simplefilter('ignore')
# just assert that we don't connect to default db
db.url = "http://localhost:7475/db/data/"
print("neo4j version: ", *db.session.neo4j_version)

from django.core.urlresolvers import reverse  # NOQA
from rest_framework import status  # NOQA
from rest_framework.test import APITestCase  # NOQA
from routes.models import Place  # NOQA


class APITests(APITestCase):

    def setUp(self):
        super(APITests, self).setUp()
        db.new_session()
        db.session.delete_all()

    def test_create_new_place(self):
        """
        Ensure we can create a new place object.
        """
        url = reverse('api:place-list')
        data = {'map': 'SP', 'name': 'Vila Mariana'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(Place.nodes.all()), 1)
        self.assertEqual(Place.nodes.all()[0].slug, 'sp--vila-mariana')
        self.assertEqual(
            response.data,
            {
                "map": "SP",
                "name": "Vila Mariana",
                "slug": "sp--vila-mariana"
            }
        )

    def test_create_new_route(self):
        vl_mariana = Place(map='SP', name='Vila Mariana').save()
        Place(map='SP', name='Av Paulista').save()

        url = reverse(
            'api:place-routes',
            kwargs={'_from': 'sp--vila-mariana', 'to': 'sp--av-paulista'}
        )

        data = {'distance': 5.1}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(vl_mariana.routes.all()), 1)
        self.assertEqual(Place.nodes.all()[0].slug, 'sp--vila-mariana')
        # Reload the json to avoid unicode keys issues
        resp_json = json.loads(json.dumps({
            "from": {
                "map": "SP",
                "name": "Vila Mariana",
                "slug": "sp--vila-mariana"
            },
            "to": {
                "map": "SP",
                "name": "Av Paulista",
                "slug": "sp--av-paulista"
            },
            "distance": 5.1
        }))
        self.assertEqual(json.loads(json.dumps(response.data)), resp_json)

    def test_get_shortest_path(self):
        A = Place(map="SP", name="A").save()
        B = Place(map="SP", name="B").save()
        C = Place(map="SP", name="C").save()
        D = Place(map="SP", name="D").save()
        E = Place(map="SP", name="E").save()
        A.routes.connect(B, {'distance': 10})
        A.routes.connect(C, {'distance': 20})
        B.routes.connect(D, {'distance': 15})
        B.routes.connect(E, {'distance': 50})
        C.routes.connect(D, {'distance': 30})

        # TODO: check what the problem with reverse... cuz resolve works:
        # 3 >>> resolve('/api/places/sp--a/sp--d/10/2.50/')
        # ResolverMatch(func=api.views.PlacesRoutesAPIView, args=(),
        #    kwargs={u'fuel': u'2.50', u'to': u'sp--d', u'_from': u'sp--a',
        #            u'autonomy': u'10'},
        #    url_name=place-routes, app_name=None, namespaces=['api']
        # )
        # url = reverse(
        #     'api:place-routes',
        #     kwargs={'_from': 'sp--a', 'to': 'sp--d',
        #             'autonomy': '10', 'fuel': '2.50'}
        # )
        url = '/api/places/sp--a/sp--d/10/2.50/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Reload the json to avoid unicode keys issues
        expected = json.loads(json.dumps({
            'autonomy': '10.00',
            'cost': '62.50',
            'distance': '25.00',
            'fuel_price': '2.50',
            'path': [
                {'map': 'SP', 'name': 'A', 'slug': 'sp--a'},
                {'map': 'SP', 'name': 'B', 'slug': 'sp--b'},
                {'map': 'SP', 'name': 'D', 'slug': 'sp--d'}
            ]
        }))
        self.assertEqual(json.loads(json.dumps(response.data)), expected)
