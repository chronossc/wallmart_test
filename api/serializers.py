# -*- coding: UTF-8 -*-

from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

from rest_framework import serializers
from routes.models import Place


class PlacesSerializer(serializers.Serializer):
    map = serializers.CharField()
    name = serializers.CharField()
    slug = serializers.CharField(read_only=True, required=False)

    def validate(self, data):
        slug = Place.slugify(data['map'], data['name'])
        try:
            Place.nodes.get(slug=slug)
        except Place.DoesNotExist:
            pass
        else:
            raise serializers.ValidationError(
                "Place with this map and name already exists."
            )
        return data

    def create(self, data):
        place = Place(**data).save()
        return place


class RoutesSerializer(serializers.Serializer):
    pass
