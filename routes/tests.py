# -*- coding: UTF-8 -*-

from __future__ import (
    absolute_import, division, print_function, unicode_literals
)
# import os
import warnings

from neomodel.core import db

warnings.simplefilter('ignore')
# just assert that we don't connect to default db
db.url = "http://localhost:7475/db/data/"
print("neo4j version: ", *db.session.neo4j_version)

from django.test import TestCase  # NOQA
from .models import Place  # NOQA


class RouteTestCase(TestCase):

    def setUp(self):
        super(RouteTestCase, self).setUp()
        db.new_session()
        db.session.delete_all()

    def test_route_finder_for_simple_routes(self):
        """
        Test shortest path
        """
        A = Place(map="SP", name="A").save()
        B = Place(map="SP", name="B").save()
        C = Place(map="SP", name="C").save()
        D = Place(map="SP", name="D").save()
        E = Place(map="SP", name="E").save()

        A.routes.connect(B, {'distance': 10})
        A.routes.connect(C, {'distance': 20})
        B.routes.connect(D, {'distance': 15})
        B.routes.connect(E, {'distance': 50})
        C.routes.connect(D, {'distance': 30})
        D.routes.connect(E, {'distance': 30})

        path = A.get_path_to(D).one
        self.assertEqual(
            [{u'map': u'SP', u'name': u'A', u'slug': u'sp--a'},
             {u'map': u'SP', u'name': u'B', u'slug': u'sp--b'},
             {u'map': u'SP', u'name': u'D', u'slug': u'sp--d'}],
            [node.properties for node in path.nodes]
        )

        path = A.get_path_to(E).one
        self.assertEqual(
            [{u'map': u'SP', u'name': u'A', u'slug': u'sp--a'},
             {u'map': u'SP', u'name': u'B', u'slug': u'sp--b'},
             {u'map': u'SP', u'name': u'E', u'slug': u'sp--e'}],
            [node.properties for node in path.nodes]
        )
