# -*- coding: UTF-8 -*-

from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

# from neo4django.db import models


# class Map(models.NodeModel):
#     name = models.StringProperty(indexed=True)


# class Route(models.NodeModel):
#     """
#     Store routes from A to B, B to C, C to D
#     """
#     map = models.Relationship(
#         Map, rel_type='owns', single=True, related_name='routes'
#     )
#     origin = models.StringProperty(indexed=True)
#     destiny = models.StringProperty(indexed=True)
#     distance = models.IntegerProperty()
#     neightboors = models.Relationship('self', rel_type='neightboors_with')

from django.template.defaultfilters import slugify

from neomodel import (
    StructuredNode, StructuredRel, StringProperty, Relationship,
    FloatProperty
)


class RouteRel(StructuredRel):
    distance = FloatProperty()


class Place(StructuredNode):
    map = StringProperty(required=True)
    name = StringProperty(required=True)
    slug = StringProperty(required=True, unique_index=True)
    routes = Relationship(b'Place', b'DISTANCE', model=RouteRel)

    def save(self):
        self.slug = Place.slugify(self.map, self.name)
        return super(Place, self).save()

    def get_path_to(self, destiny):
        results, columns = self.cypher(
            b'START beginning=node({self}), end=node({destiny}) MATCH step = shortestPath(beginning-[*..100]-end) RETURN step',  # NOQA
            params={'destiny': destiny._id}
        )
        return results
        return [self.inflate(row[0]) for row in results]

    @staticmethod
    def slugify(map, name):
        return "--".join([slugify(map), slugify(name)])
