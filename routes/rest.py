# -*- coding: UTF-8 -*-

from __future__ import (
    absolute_import, division, print_function, unicode_literals
)

from rest_framework.serializers import ModelSerializer
from rest_framework.viewsets import ModelViewSet

from .models import Route


class RouteSerializer(ModelSerializer):
    class Meta:
        model = Route
        fields = ('origin', 'destiny', 'distance')


class RouteViewSet(ModelViewSet):
    queryset = Route.objects.all()
    serializer_class = RouteSerializer
    # filter_backends = (filters.DjangoFilterBackend,)
    # filter_fields = ('origin', 'destiny', 'distance')
